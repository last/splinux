#!/bin/bash
dialog --title "Splinux" --yesno "Dieses Skript wird Ihr System zum Splinux konfigurieren. Möchten Sie fortfahren?" 7 60
response=$?
case $response in
   0) ;;
   1) exit;;
   255) exit;;
esac 
dialog --title "Splinux" --msgbox "Paketlisten werden gelesen" 7 60
sudo apt-get update
dialog --title "Splinux" --msgbox "Zusätzliche Software wird installiert" 7 60
echo -e "\033[32mGlasgow Haskell Compiler wird installiert\033[0m"
sudo apt-get install ghc
echo -e "\033[32mNetwide Assembler wird installiert\033[0m"
sudo apt-get install nasm
echo -e "\033[32mGNU Compiler Collection wird installiert\033[0m"
sudo apt-get install gcc
echo -e "\033[32mGNU C++ wird installiert\033[0m"
sudo apt-get install g++
echo -e "\033[32mJava Runtime Environment wird installiert\033[0m"
sudo apt-get install openjdk-7-jre
echo -e "\033[32mJava Development Kit wird installiert\033[0m"
sudo apt-get install openjdk-7-jdk
echo -e "\033[32mAdobe Flash wird installiert\033[0m"
sudo apt-get install adobe-flashplugin
echo -e "\033[32mMultimediacodecs werden installiert\033[0m"
sudo apt-get install ubuntu-restricted-extras
echo -e "\033[32mEclipse wird installiert\033[0m"
sudo apt-get install eclipse
echo -e "\033[32mGo programming language wird installiert\033[0m"
sudo apt-get install golang
echo -e "\033[32mGeany wird installiert\033[0m"
sudo apt-get install geany
echo -e "\033[32mVim wird installiert\033[0m"
sudo apt-get install vim
dialog --title "Software" --msgbox "Optionale Programme:\n1) Wine (Windowsprogrammstarter)\n2) Tex Live (LaTeX)\n3) Emacs\n4) Qt Creator ( C++/Qt-IDE)\n5) VLC Media Player" 30 80
cmd=(dialog --separate-output --checklist "Select options:" 22 76 16)
options=(1 "Wine" on
         2 "Tex Live" on
         3 "Emacs" on
         4 "Qt Creator" on
	 5 "VLC" on)
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
    case $choice in
        1)
        	echo -e "\033[32mWine wird installiert\033[0m"
			sudo apt-get install wine
			sudo apt-get install winetricks
            	;;
        
        2)
		echo -e "\033[32mTex Live (LaTeX) wird installiert\033[0m"
			sudo apt-get install texlive texlive-lang-german texlive-doc-de texlive-latex-extra
		;;
	3)
		echo -e "\033[32mEmacs wird installiert\033[0m"
			sudo apt-get install emacs
		;;
        4)
		echo -e "\033[32mQt Online Installer for Linux wird heruntergeladen\033[0m"
			cd /tmp
			wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
			sudo chmod +x qt-opensource-linux-x64-online.run
		echo -e "\033[32mQt Creator wird installiert\033[0m"
			./qt-opensource-linux-x64-online.run
		echo -e "\033[32mQt Online Installer for Linux wird gelöscht\033[0m"
			rm qt-opensource-linux-x64-online.run
			cd
		;;
	5)
		echo -e "\033[32mVLC wird installiert\033[0m"
	       		sudo apt-get install vlc
		;;	
    esac
done
